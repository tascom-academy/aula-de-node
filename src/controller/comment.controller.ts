import {Request, Response} from 'express'
import { CommentService } from '../service/index.service'

export const createCommentController = async (req: Request, res: Response) => {
  const body = req.body
  const post = await CommentService.createCommentService(body)
  res.status(201).json(post)
}
