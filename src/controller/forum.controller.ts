import { Request, Response } from 'express'
import { ForumService } from '../service/index.service'

/** Lista todos os forums */
export const listAllForumPostsController = async (req: Request, res: Response) =>{
  const { per_page, page } = req.query
  const forum = await ForumService.listAllForumPostsService(String(per_page), String(page))
  res.status(200).json(forum)
}

/** Busca um forum por nome */
export const forumPostsByNameController = async (req: Request, res: Response) =>{
  const { per_page, page } = req.query
  const { forumName } = req.params
  const forum = await ForumService.forumPostsByNameService(String(forumName), String(per_page), String(page))
  res.status(200).json(forum)
}