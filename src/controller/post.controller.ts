import {Request, Response} from 'express'
import { PostService } from '../service/index.service'

/** Cria um post */
export const createPostController = async (req: Request, res: Response) => {
  const body = req.body
  const post = await PostService.createPostService(body)
  res.status(201).json(post)
}

/** Busco um post por id */
export const getPostByIdController = async (req: Request, res: Response) => {
  const {postId} = req.params
  const post = await PostService.getPostByIdService(postId)
  res.status(200).json(post)
}

/** Fixa um post por id */
export const pinPostByIdController = async (req: Request, res: Response) => {
  const {postId} = req.params
  const post = await PostService.pinPostByIdService(postId)
  res.status(200).json(post)
}

/** Trava um post por id */
export const lockPostByIdController = async (req: Request, res: Response) => {
  const {postId} = req.params
  const post = await PostService.lockPostByIdService(postId)
  res.status(200).json(post)
}