import { Request, Response } from "express"
import { AuthService } from "../service/index.service"

export const login = async (req: Request, res: Response) => {
  const body = req.body
  const login = await AuthService.loginService(body)
  return res.status(200).json(login)
}

/** Realiza logout (não faz nada no momneto) */
export const logout = async (req: Request, res: Response) => {
  return res.status(200).json({})
}

/** Verifica se o usuário ainda tem um login válido */
export const loggedIn = async (req: Request, res: Response) => {
  const login = await AuthService.loggedInService(req.user, String(req.headers.authorization))
  return res.status(200).json(login)
}

/** Verifica o token enviado ao email */
export const verifyEmail = async (req: Request, res: Response) => {
  const query = req.query
  const html = await AuthService.verifyEmailService(String(query.token))
  return res.status(200).send(html)
}