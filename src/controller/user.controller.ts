import { Request, Response } from "express"
import { UserService, EmailService } from "../service/index.service"

/** GET - Buscar a lista de usuários */
const getAllUsers = async (req: Request, res: Response) => {
  const listUsers = await UserService.listAllUsersService()
  res.status(200).json(listUsers)
}

const getAllUsersV2 = async (req: Request, res: Response) => {
  const listUsers = await UserService.listAllUsersServiceV2()
  res.status(200).json(listUsers)
}

/** DELETE - Deleta usuário por id */
const deleteUserById = async (req: Request, res: Response) => {
  const {userId} = req.params
  await UserService.deleteUserByIdService(userId)
  res.status(204).json()
}

/** GET - Buscar usuário por id */
const getUserById = async (req: Request, res: Response) => {
  const {userId} = req.params
  const user = await UserService.getUserByIdService(userId)
  res.status(200).json(user)
}

const getUserByIdV2 = async (req: Request, res: Response) => {
  const {userId} = req.params
  const user = await UserService.getUserByIdServiceV2(userId)
  res.status(200).json(user)
}

/** POST - Criar usuário */
const creatUser = async (req: Request, res: Response) => {
  const body = req.body
  const user = await UserService.createUserService(body)
  res.status(201).json(user)
}

const creatUserV2 = async (req: Request, res: Response) => {
  const body = req.body
  const user = await UserService.createUserServiceV2(body)
  await EmailService.sendVerifyEmail(user.name, user.email)
  res.status(201).json(user)
}

/** PUT - Atualizar usuário por id */
const updateUserById = async (req: Request, res: Response) => {
  const {userId} = req.params
  const body = req.body
  const user = await UserService.updateUserByIdService(userId, body)
  res.status(200).json(user)
}

const updateUserByIdV2 = async (req: Request, res: Response) => {
  const {userId} = req.params
  const body = req.body
  const user = await UserService.updateUserByIdServiceV2(userId, body)
  res.status(200).json(user)
}

/** Realiza o upload da imagem de perfil do usuário */
const uploadUserProfileImage = async (req: Request, res: Response) => {
  const {userId} = req.params
  const user = await UserService.uploadUserProfileImageService(userId)
  res.status(200).json(user)
}

export {
  getAllUsers,
  deleteUserById,
  getUserById,
  creatUser,
  updateUserById,
  creatUserV2,
  updateUserByIdV2,
  getUserByIdV2,
  getAllUsersV2,
  uploadUserProfileImage
}