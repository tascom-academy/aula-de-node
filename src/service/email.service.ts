import nodemailer from "nodemailer"
import * as Handlebars from "handlebars"
import * as fs from "fs"
import * as path from "path"
import { generateTokenJWT } from "../config/authentication/strategy.passport"
import dotenv from 'dotenv'
dotenv.config()

export async function sendVerifyEmail(name:string, to: string) {
    const token = await generateTokenJWT({email: to}, 3600)
    const data = {
        name,
        url: process.env.EMAIL_URL + `?token=${token}`
    }
    /** Busca o template html verify-email  */
    let template = "verify-email"
    template = path.join(__dirname, "../template/" + template + ".html")
    const templateContent: any = fs.readFileSync(template)
    /** Modifica as variaveis no html */
    const compiledTemplate = Handlebars.compile(templateContent.toString())(data)
    await sendEmail(
    'tascom.academy@gmail.com',
    to,
    'Confirmação de email',
    compiledTemplate
  )
}

/** Fução para enviar o email */
export async function sendEmail(from: string, to: string, subject: string, html: string) {
    let transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: false,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
      },
    });
  
    let info = await transporter.sendMail({
      from,
      to,
      subject,
      html,
    });
   
    console.log(`Menssagem enviada: ${info.messageId}`);
  }