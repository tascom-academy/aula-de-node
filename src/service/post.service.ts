import mongoose from "mongoose";
import { PostModel } from "../model/post.model";
import { PostRepository } from "../schema/post.schema";

export const createPostService = async (post: PostModel) => {
  return await PostRepository.create(post)
}

/** Fixa um post */
export const pinPostByIdService = async (id: string) => {
  /** Busca o post no banco */
  const post = await PostRepository.findOne({_id: new mongoose.Types.ObjectId(id)}, {is_pinned: false})
  .select('is_pinned')
  /** Atualiza o campo is_pinned */
  await PostRepository.findOneAndUpdate({_id: new mongoose.Types.ObjectId(id)}, {is_pinned: !post?.is_pinned})
  return await getPostByIdService(id)
}

/** Trava um post */
export const lockPostByIdService = async (id: string) => {
  /** Busca o post no banco */
  const post = await PostRepository.findOne({_id: new mongoose.Types.ObjectId(id)}, {is_locked: false})
  .select('is_locked')
  /** Atualiza o campo is_locked */
  await PostRepository.findOneAndUpdate({_id: new mongoose.Types.ObjectId(id)}, {is_locked: !post?.is_locked})
  return await getPostByIdService(id)
}

export const getPostByIdService = async (id: string) => {
  const post = await PostRepository.aggregate([
    /** Busca os post por id */
    {
      $match: {_id: new mongoose.Types.ObjectId(id)}
    },
    /** Busca o usuário na table de user */
    {
      $lookup: {
        from: 'users',
        as: 'user',
        localField: 'author_id',
        foreignField: '_id'
      }
    },
    /** Busca o foruum na table de forums */
    {
      $lookup: {
        from: 'forums',
        as: 'forum',
        localField: 'forum_id',
        foreignField: '_id'
      }
    },
    /** converte os array para objeto, a função lookup retorno um array */
    {
      $addFields: {
        user: {$arrayElemAt : ['$user', 0]},
        forum: {$arrayElemAt : ['$forum', 0]},
      }
    },
    /** adiciona os campos nescessarios para o front funcionar */
    {
      $addFields: {
        author: '$user.name',
        forum: '$forum.name'
      }
    },
    /** busca os comentarios na tabela comments */
    {
      $lookup: {
        from: 'comments',
        as: 'comments',
        localField: '_id',
        foreignField: 'post_id'
      }
    },
    {
      $lookup: {
        from: 'comments',
        as: 'comments',
        let: {post_id: '$_id'},
        pipeline: [
          {
            $match: {
              $expr: { $eq: ["$post_id", '$$post_id'] },
            }
          },
          {
            $lookup: {
              from: 'users',
              as: 'user',
              localField: 'author_id',
              foreignField: '_id'
            }
          },
          {
            $addFields: {
              user: {$arrayElemAt : ['$user', 0]},
            }
          },
        ]
      }
    },
    /** adiciona os campos do front */
    {
      $addFields: {
        post: '$$ROOT',
        comments: '$comments',
      }
    }
  ])
  return post[0]
}