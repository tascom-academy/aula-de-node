import { ForumRepository } from "../schema/forum.schema"

export const listAllForumPostsService = async (per_page: string, page: string) => {
  const forums = await ForumRepository.aggregate([
    /** Busca na tabela de posts */
    {
      $lookup: {
        from: "posts",
        let: { forum_id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $eq: ["$forum_id", '$$forum_id'] },
            }
          },
          {
            $lookup: {
              from: 'users',
              as: 'user',
              localField: 'author_id',
              foreignField: '_id'
            }
          },
          {
            $addFields: {
              user: {$arrayElemAt : ['$user', 0]},
            }
          },
          {
            $addFields: {
              author: '$user.name',
            }
          }
        ],
        as: "posts"
      }
    },
    /** Ordena por _id */
    {
      $sort: {_id: 1 } 
    },
    /** Paginação */
    { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: Number(page) } } ],
      data: [ { $skip: Number(per_page) * (Number(page) - 1) }, { $limit: Number(per_page) } ]
    }},
    /** Projeta os campos de retorno da query */
    {
      $project: {
        forums: '$data',
        page: {$arrayElemAt : ['$metadata.page', 0]},
        total: {$arrayElemAt : ['$metadata.total', 0]},
      }
    },
    /** Busca na tabela post pelos posts fixados */
    {
      $lookup: {
        from: "posts",
        let: { is_pinned: true },
        pipeline: [
          {
            $match: {
              $expr: { $eq: ["$$is_pinned", "$is_pinned"] },
            }
          }
        ],
        as: "pinned_posts"
      }
    },
    /** Adiciona o campo per_page na query*/
    {
      $addFields: {
        per_page: Number(per_page),
      }
    }
])
  return forums[0]
}

export const forumPostsByNameService = async (name: string, per_page: string, page: string) => {
  const forums = await ForumRepository.aggregate([
    {
      $match: {name: name}
    },
    /** Busca na tabela de posts */
    {
      $lookup: {
        from: "posts",
        let: { forum_id: '$_id' },
        pipeline: [
          {
            $match: {
              $expr: { $eq: ["$forum_id", '$$forum_id'] },
            }
          },
          {
            $lookup: {
              from: 'users',
              as: 'user',
              localField: 'author_id',
              foreignField: '_id'
            }
          },
          {
            $addFields: {
              user: {$arrayElemAt : ['$user', 0]},
            }
          },
          {
            $addFields: {
              author: '$user.name',
            }
          }
        ],
        as: "posts"
      }
    },
    /** Ordena por _id */
    {
      $sort: {_id: 1 } 
    },
    /** Paginação */
    { '$facet'    : {
      metadata: [ { $count: "total" }, { $addFields: { page: Number(page) } } ],
      data: [ { $skip: Number(per_page) * (Number(page) - 1) }, { $limit: Number(per_page) } ]
    }},
    /** Projeta os campos de retorno da query */
    {
      $project: {
        forum: {$arrayElemAt : ['$data', 0]},
        page: {$arrayElemAt : ['$metadata.page', 0]},
        total: {$arrayElemAt : ['$metadata.total', 0]},
      }
    },
    /** Adiciona o campo per_page na query*/
    {
      $addFields: {
        per_page: Number(per_page),
      }
    }
])
  return forums[0]
}