import mongoose from "mongoose";
import { CommentModel } from "../model/comment.model";
import { CommentsRepository } from "../schema/comments.schema";

/** Cria um comentario */
export const createCommentService = async (comment: CommentModel) => {
  const _comment = await CommentsRepository.create(comment)
  const comments = await CommentsRepository.aggregate([
    /** Busca o comentario por post_id */
    {
      $match: {post_id: new mongoose.Types.ObjectId(comment.post_id)}
    },
    /** Busca o usuário na  tabela de users */
    {
      $lookup: {
        from: 'users',
        as: 'user',
        localField: 'author_id',
        foreignField: '_id'
      }
    },
    /** Busca o index 0 no array user */
    {
      $addFields: {
        user: {$arrayElemAt : ['$user', 0]},
      }
    },
])
  return {
    comment: _comment,
    comments
  }
}
