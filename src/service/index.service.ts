import * as UserService from './user.service'
import * as AuthService from './auth.service'
import * as EmailService from './email.service'
import * as ForumService from './forum.service'
import * as PostService from './post.service'
import * as CommentService from './comment.service'

export {
  UserService,
  AuthService,
  EmailService,
  ForumService,
  PostService,
  CommentService
}