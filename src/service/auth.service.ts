import { AuthModel } from "../model/auth.model"
import { UserModel } from "../model/user.model"
import { UserRepository } from "../schema/user.schema"
import { SanitizeObj } from "../utils/sanitize-obj"
import { validateClassObj } from "../utils/validate-class-obj"
import * as argon from 'argon2'
import { AppError } from "../model/erros.model"
import { HttpStatus } from "../utils/http-status"
import { generateTokenJWT, verifyToken } from "../config/authentication/strategy.passport"
import * as Handlebars from "handlebars"
import * as fs from "fs"
import * as path from "path"
import mongoose from "mongoose"

const validateAuth = async(auth: AuthModel): Promise<AuthModel> => {
  const authInstance = await SanitizeObj<AuthModel>(auth, AuthModel, ['password'])
  await validateClassObj(authInstance, 'Erro ao validar os campos do login')
  return authInstance
}

const validatePassword = async (user: UserModel | null, password: string): Promise<void> => {
  let validate = false
  if (user) {
    validate = await argon.verify(user.password, password + user.email)
  }
  if (!validate) throw new AppError('Email ou senha incorreta', HttpStatus.UNAUTHORIZED)
}

export const loginService = async (body: AuthModel) => {
  body = await validateAuth(body)
  let user = await UserRepository.findOne({email: body.email})
    .select('email password name verify can_post can_comment age createdAt updatedAt profile_image')
  await validatePassword(user, body.password)
  if (!user?.verify) throw new AppError('Usuário não confirmado', HttpStatus.UNAUTHORIZED) 
  const token = await generateTokenJWT({email: user?.email, id: user?._id, username: user?.name })
  return {
    ...user?.toJSON(),
    username: user?.name,
    id: user?._id,
    token: `Bearer ${token}`,
    logged_in: true,
  }
}

export const loggedInService = async (payload: any, token: string) => {
  const user = await UserRepository.findOne({_id: new mongoose.Types.ObjectId(payload?.id)})
  return {
    ...user?.toJSON(),
    username: payload?.username,
    id: payload?.id,
    token,
    logged_in: true,
  }
}

/** validação do token enviado ao email */
export const verifyEmailService = async (token: string) => {
  const payload = await verifyToken(token)
  const email = payload?.email
  if (email) {
    /** modifica o campo verify para true */
    await UserRepository.findOneAndUpdate({email}, {verify:true})
  }
  const data = {
    url: process.env.EMAIL_SUCCESS_URL
  }
  /** gera o html que o cliente vai abrari ao clicar no botão do email */
  let template = "verify-email-success"
  template = path.join(__dirname, "../template/" + template + ".html")
  const templateContent: any = fs.readFileSync(template)
  /** Modifica as variaveis no html */
  const compiledTemplate = Handlebars.compile(templateContent.toString())(data)
  return compiledTemplate
}