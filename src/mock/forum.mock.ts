import { ForumModel } from "../model/index.user"

/** Mock do forum */
export const ForumMock: ForumModel[] = [
  {
    _id: '643ab2977ab400d276027104',
    name: 'Filmes',
  },
  {
    _id: '643ab2a0efbe56c3678eac26',
    name: 'Jogos',
  },
  {
    _id: '643ab2b18f1040505ce823e5',
    name: 'Música',
  },
  {
    _id: '643ab2b5ed15642a32c2fc05',
    name: 'Programação',
  },
  {
    _id: '643ab2b818b0ce65129d96b8',
    name: 'Livros',
  },
  {
    _id: '643ab2ba9ada8b7480651b78',
    name: 'Discord',
  },
  {
    _id: '643ab2bc1af4ab40714abf1f',
    name: 'ChatGPT',
  },
]