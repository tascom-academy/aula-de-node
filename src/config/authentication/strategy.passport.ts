import passport from 'passport'
import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt'
import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
dotenv.config()
/** Tempo de expiração do token */
const EXPIRATION_TIME = 60 * 60 * 4 // 4 HORAS

/** Senha do JWT */
const JWT_SECRET_KEY = process.env.JWT_KEY || 'batata'

export const initializePassport = () => {
  /** Opções da estrategia do passport */
  const opts: StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: JWT_SECRET_KEY
  }

  /** Criação da estrategia do passport */
  const strategy = new Strategy(opts, (payload, done) => {
    try {
      if (payload.email) {
        return done(null, payload)
      }
      return done(null, false)
    } catch (error) {
      return done(error, false)
    }
  })

  /** Inicialização do passport */
  passport.use(strategy)
  passport.initialize()
}

export const authenticate = () => passport.authenticate('jwt', { session: false })
export const generateTokenJWT = async (payload: any, expiresIn: number = EXPIRATION_TIME) => {
  return jwt.sign(payload, JWT_SECRET_KEY, { expiresIn })
}

/** Função para validar o token JWT */
export const verifyToken = async (token: string): Promise<any> => {
  return jwt.verify(token, JWT_SECRET_KEY)
}