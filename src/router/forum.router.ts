import { Router } from 'express'
import { ForumController } from '../controller/index.controller'

export const forumRouter = Router()

forumRouter.get('/forum', [], ForumController.listAllForumPostsController)
/** Busca um forum por nome */
forumRouter.get('/forum/:forumName', [], ForumController.forumPostsByNameController)