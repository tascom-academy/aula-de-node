import { Router } from "express"
import { AuthController } from "../controller/index.controller"
import { authenticate } from "../config/authentication/strategy.passport"

const authRouter = Router()

authRouter.post('/login', [], AuthController.login)
/** Faz logout */
authRouter.patch('/logout', [authenticate()], AuthController.logout)
/** Válida usuário logado */
authRouter.get('/logged-in', [authenticate()], AuthController.loggedIn)
/** Verifica o token enviado ao email */
authRouter.get('/verify-email', [], AuthController.verifyEmail)

export {
  authRouter
}