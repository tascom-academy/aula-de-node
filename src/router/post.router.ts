import { Router } from 'express'
import { PostController } from '../controller/index.controller'
import { authenticate } from '../config/authentication/strategy.passport'

export const postRouter = Router()

postRouter.post('/post', [authenticate()], PostController.createPostController)
/** Busca um post por id */
postRouter.get('/post/:postId', [], PostController.getPostByIdController)
/** Fixa um post por id */
postRouter.patch('/posts/:postId/pin_post', [authenticate()], PostController.pinPostByIdController)
/** Trava um post por id */
postRouter.patch('/posts/:postId/lock_post', [authenticate()], PostController.lockPostByIdController)