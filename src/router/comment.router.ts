import { Router } from 'express'
import { CommentController } from '../controller/index.controller'
import { authenticate } from '../config/authentication/strategy.passport'

export const commentRouter = Router()

commentRouter.post('/comment', [authenticate()], CommentController.createCommentController)
