import { Router } from "express"
import { UserController } from "../controller/index.controller"
import { authenticate } from "../config/authentication/strategy.passport"
import multer from 'multer'

/** Pacote que salva a imagem na maquina */
const storage = multer.diskStorage({
  destination: 'public/',
  /** Configuração para o nome do arquivo que no nosso caso será o id do usuário */
  filename: function (req, file, cb) {
    const user = req?.user as any
    const id = user.id
    if (!id) return cb(new Error('Arquivo inválido'), '')
    const filename = id + '.jpeg'
    return cb(null, filename)
  }
})
const upload = multer({ storage })

const userRouter = Router()

userRouter.get('/user', [authenticate()], UserController.getAllUsers)
userRouter.delete('/user/:userId', [], UserController.deleteUserById)
userRouter.get('/user/:userId', [], UserController.getUserById)
userRouter.post('/user', [], UserController.creatUser)
userRouter.put('/user/:userId', [], UserController.updateUserById)

const userRouterV2 = Router()
userRouterV2.post('/user', [], UserController.creatUserV2)
userRouterV2.get('/user', [], UserController.getAllUsersV2)
/** Atualiza a imagem de perfil do usuário
 * Tem dois middleware aqui o de autenticação e o que salva a imagem
 */
userRouterV2.patch('/user/:userId/upload-profile-image', [authenticate(), upload.single('user[profile_image]')], UserController.uploadUserProfileImage)
userRouterV2.put('/user/:userId', [authenticate()], UserController.updateUserByIdV2)
userRouterV2.get('/user/:userId', [], UserController.getUserByIdV2)

export {
  userRouter,
  userRouterV2
}