import { Expose } from 'class-transformer'
import { IsString, IsBoolean, IsNumber, IsNotEmpty, IsOptional, Min, IsEmail} from 'class-validator'
import {Menssage} from '../config/menssages/default-menssages'

export class PostModel {
  @Expose({name: 'id'})
  @IsString({message: Menssage.isString})
  @IsOptional()
  _id?: string
  
  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  title!: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  body!: string
  
  @Expose()
  @IsBoolean({message: Menssage.isBollean})
  @IsOptional()
  is_pinned?: boolean

  @Expose()
  @IsBoolean({message: Menssage.isBollean})
  @IsOptional()
  is_locked?: boolean

  @Expose()
  @IsString({message: Menssage.isString})
  @IsOptional()
  subforum_id?: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  forum_id?: string

  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  author_id?: string
}