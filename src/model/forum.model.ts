import { Expose } from 'class-transformer'
import { IsString, IsBoolean, IsNotEmpty, IsOptional} from 'class-validator'
import {Menssage} from '../config/menssages/default-menssages'

export class ForumModel {
  @Expose({name: 'id'})
  @IsString({message: Menssage.isString})
  @IsOptional()
  _id?: string
  
  @Expose()
  @IsString({message: Menssage.isString})
  @IsNotEmpty({message: Menssage.isNotEmpty})
  name!: string
  
  @Expose()
  @IsBoolean({message: Menssage.isBollean})
  @IsOptional()
  admin_only?: boolean

  @Expose()
  @IsBoolean({message: Menssage.isBollean})
  @IsOptional()
  admin_view_only ?: boolean
}