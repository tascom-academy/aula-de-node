import { Schema, model } from 'mongoose'
import { Menssage } from '../config/menssages/default-menssages'

const ForumSchema = new Schema({
  /** Nome do forum */
  name: {
    type: String,
    required: [true, Menssage.mongoRequired('nome')],
    uppercase: true
  },

  /** Só adiministrador realiza ações no forum */
  admin_only: {
    type: Boolean,
    default: false,
  },

  /** Só adiministrador ver o forum */
  admin_view_only: {
    type: Boolean,
    default: false,
  },
}, {
  strict: true,
  timestamps: true,
  versionKey: false
})

/** Index */
/** Não deixa criar forum com nome repetido */
ForumSchema.index({name: 1}, {unique: true})

/** Middleware */

export const ForumRepository = model('Forum', ForumSchema)
ForumRepository.syncIndexes()
